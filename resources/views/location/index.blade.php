@extends('layouts.app')

@section('content')
    <div data-module="location" class="col-md-4">
        <div class="form-group">
            <label for="country">Country: </label>
            <select name="" class="form-control" id="country">
                @foreach($countries as $country)
                    <option value="{{ $country->code }}">{{ $country->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="zip_code">Zip code:</label>
            <input type="text" class="form-control" id="zip_code">
        </div>
        <button type="submit" class="btn btn-success" id="get_location">Submit</button>

        <div class="col-md-12" id="result"></div>
    </div>
@endsection