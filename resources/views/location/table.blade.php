<table class="table table-striped">
    <thead>
        <tr>
            <th>Zip Code</th>
            <th>Name</th>
            <th>Latitude</th>
            <th>Longitude</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $location->zip_code }}</td>
            <td>{{ $location->place_name }}</td>
            <td>{{ $location->latitude }}</td>
            <td>{{ $location->longitude }}</td>
        </tr>
    </tbody>
</table>