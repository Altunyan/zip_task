<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('/dist/main.css') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" id="meta_csrf" content="{{ csrf_token() }}">

    <title>Location</title>
</head>
<body>
@yield('content')
</body>
<script src="{{ url('/dist/main.js') }}"></script>
</html>