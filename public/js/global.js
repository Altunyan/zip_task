//html elements creator
window.generateTag = (elName, classNamesArr, inner, id, attributesObj) => {

    let element = document.createElement(elName);

    if (classNamesArr && classNamesArr.length > 0) {
        let classes = classNamesArr.join(' ');
        element.className = classes;
    }

    if (id) {
        element.id = id;
    }

    if (inner) {
        if (inner.nodeName) {
            element.append(inner);
        } else {
            element.innerHTML = inner;
        }
    }

    if (attributesObj) {
        for (let key in attributesObj) {
            element.setAttribute(key, attributesObj[key]);
        }
    }

    element.appendNode = function (child) {
        this.append(child);
        return this;
    };

    return element;
};

window.addStyles = (el, styles_obj) => {
    if (el.length > 0) {
        for (let i = 0, len = el.length; i < len; i++) {
            for (let s in styles_obj) {
                el[i].style[s] = styles_obj[s];
            }
        }
    } else {
        for (let j in styles_obj) {
            el.style[j] = styles_obj[j];
        }
    }
};