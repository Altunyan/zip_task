import 'bootstrap';
import './global';
//importing js from modules

import initLocation from './modules/Location';

class Main {
    constructor() {
        initLocation();
    }
}

new Main();