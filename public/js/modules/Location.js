const xhrPromise = require('xhr-promise');

import Notification from '../utils/Notification';

class Location {
    constructor() {
        this.token = document.getElementById('meta_csrf').getAttribute('content');
        this.country = document.getElementById('country');
        this.zip_code = document.getElementById('zip_code');
        this.get_location_btn = document.getElementById('get_location');
        this.result_elem = document.getElementById('result');

        this.get_location_btn.addEventListener('click', (e) => {
            let country_code = this.country.value,
                zip_code = this.zip_code.value;

            if(country_code && zip_code) {
                this.getLocation(country_code, zip_code)
            } else {
                Notification.show('Need to fill in all fields', 'error')
            }
        });
    }

    getLocation(country_code, zip_code) {
        new xhrPromise().send({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': this.token,
                'Content-Type': 'application/json'
            },
            url: './get_location',
            data: JSON.stringify({
                country_code: country_code,
                zip_code: zip_code,
            }),
        })
            .then((res) => {
                let response = res.responseText,
                    status = response.status;

                this.result_elem.innerHTML = '';

                if(status === 200) {
                    this.result_elem.innerHTML = response.view;
                } else {
                    Notification.show(response.message, 'error')
                }
            })
    }

    static _init() {
        let container = document.querySelector('[data-module="location"]');

        if (container) {
            return new Location();
        }
    }
}

export default Location._init;



