class Notification {
    // showing response notifications
    static show(message, type = 'default') {
        const notification_block = generateTag('div', ['notification_block']),
            text = generateTag('p', ['notification_text']);
        text.innerText = message;

        if (type === 'success') {
            addStyles(notification_block, {'backgroundColor': '#4BB543'})
        } else if (type === 'error') {
            addStyles(notification_block, {'backgroundColor': '#AF0606'})
        } else {
            addStyles(notification_block, {'backgroundColor': '#147efb'})
        }

        const showMessage = () => {
            addStyles(notification_block, {'bottom': '0'});
            hideMessage();
        };

        const hideMessage = () => {
            setTimeout(() => {
                addStyles(notification_block, {'bottom': '-100%'});
            }, 3000);
            destroyNotification(notification_block);
        };

        const destroyNotification = (element) => {
            setTimeout(() => {
                element.remove()
            }, 4000)
        };

        setTimeout(showMessage, 1000);

        notification_block.appendChild(text);
        document.body.appendChild(notification_block);
    };
}

export default Notification;