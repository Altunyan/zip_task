# Documentation
- Install composer
    ```bash
        composer install
    ```   
- Create .env file in root directory for runing laravel.
- Copy all from .env.example and add to .env.
- Generate app key for laravel 
    ```bash
       php artisan key:generate
    ```
- After key generation add db configs in .env
- Create tables in db
    ```bash
       php artisan migrate
    ```
- Add countries to table with seeder
    ```bash
       php artisan db:seeder --class=CountriesTableSeeder
    ```
- Install npm
    ```bash
        npm install
    ```
- After installation npm run webpack
    ```bash
        npm run build
    ```