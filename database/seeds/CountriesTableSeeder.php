<?php

use App\Facades\CountryIO;
use App\Models\MySQL\Country;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::truncate();

        $countries = CountryIO::getCountryNames();
        $countries_for_db = array_flip([
            'Argentina',
            'Austria',
            'Australia',
            'Belgium',
            'Brazil',
            'Germany',
            'Denmark',
            'Spain',
            'United Kingdom',
            'Italy',
        ]);


        foreach ($countries as $code => $name) {
            if(isset($countries_for_db[$name])) {
                $country = new Country();

                $country->name = $name;
                $country->code = $code;

                $country->save();
            }
        }
    }
}
