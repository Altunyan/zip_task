<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 21.07.2019
 * Time: 11:02
 */

namespace App\Services;


use GuzzleHttp\Client;

class CountryIOService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * CountryIOService constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://country.io/',
        ]);
    }

    /**
     * Get countries
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCountryNames()
    {
        $response = $this->client->request('GET', '/names.json');

        $names = json_decode($response->getBody()->getContents());

        return $names;
    }
}