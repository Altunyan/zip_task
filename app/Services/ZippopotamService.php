<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 20.07.2019
 * Time: 22:26
 */

namespace App\Services;

use GuzzleHttp\Client;

class ZippopotamService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * ZippopotamService constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://api.zippopotam.us/',
        ]);
    }

    /**
     * Get location information by zip code and country code
     *
     * @param $country_code
     * @param $zip_code
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLocation($country_code, $zip_code)
    {
        $response = $this->client->request('GET', "/$country_code/$zip_code");

        $location = json_decode($response->getBody()->getContents(), true);

        return $location;
    }
}