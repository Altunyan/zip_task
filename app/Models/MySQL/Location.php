<?php

namespace App\Models\MySQL;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'country_id',
        'zip_code',
        'place_name',
        'longitude',
        'latitude',
        'state',
        'state_abbreviation',
    ];
}
