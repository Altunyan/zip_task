<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 20.07.2019
 * Time: 22:26
 */

namespace App\Providers;


use App\Services\ZippopotamService;
use Illuminate\Support\ServiceProvider;

class ZippopotamServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind('ZippopotamService', function () {
            return new ZippopotamService();
        });
    }
}