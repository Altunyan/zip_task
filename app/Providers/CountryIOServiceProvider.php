<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 21.07.2019
 * Time: 11:01
 */

namespace App\Providers;


use App\Services\CountryIOService;
use Illuminate\Support\ServiceProvider;

class CountryIOServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind('CountryIOService', function() {
            return new CountryIOService();
        });
    }
}