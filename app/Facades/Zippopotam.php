<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 20.07.2019
 * Time: 22:24
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Zippopotam extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ZippopotamService';
    }
}