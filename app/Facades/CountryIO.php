<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 21.07.2019
 * Time: 10:59
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class CountryIO extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'CountryIOService';
    }
}