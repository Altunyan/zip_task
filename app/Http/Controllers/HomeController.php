<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 21.07.2019
 * Time: 11:30
 */

namespace App\Http\Controllers;

use App\Facades\Zippopotam;
use App\Models\MySQL\Country;
use App\Models\MySQL\Location;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    public function index()
    {
        $countries = Country::all();

        return view('location.index')->with(['countries' => $countries]);
    }

    /**
     * Getting information for zip code
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocation(Request $request)
    {
        try {
            $country_code = $request->country_code;
            $zip_code = $request->zip_code;

            $country = Country::where('code', $country_code)->firstOrFail();

            $location = Location::where('country_id', $country->id)
                ->where('zip_code', $zip_code)
                ->first();

            if(!$location) {
                $zippopotam = Zippopotam::getLocation($country_code, $zip_code);
                $place = $zippopotam['places'][0];

                $location = new Location();

                $location->zip_code = $zippopotam['post code'];
                $location->country_id = $country->id;
                $location->place_name = $place['place name'];
                $location->longitude = $place['longitude'];
                $location->state = $place['state'];
                $location->state_abbreviation = $place['state abbreviation'];
                $location->latitude = $place['latitude'];

                $location->save();
            }

            $view = View::make('location.table')->with(['location' => $location])->render();

            $response = [
                'status' => Response::HTTP_OK,
                'view' => $view,
            ];
        } catch (ModelNotFoundException $e) {
            $response = [
                'status' => Response::HTTP_NOT_FOUND,
                'message' => 'Country with this code not exist'
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => $e->getCode() == 404 ? 'Nothing was found' : 'Oops, something went wrong',
            ];
        }

        return response()->json($response, $response['status']);
    }
}